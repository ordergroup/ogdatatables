# -*- coding: utf-8 -*-
from ogdatatables.table import Datatable
from .models import Article
from django.template import loader as template_loader


class ArticleListDatatable(Datatable):
    columns = (
        (u'tytuł', 'title'),
        (u'opis', 'description')
    )
    queryset = Article.objects.all()
    row_template = template_loader.get_template('app/row.html')
