from app.tables import ArticleListDatatable
from django.views.generic.base import TemplateView


class ArticleView(TemplateView):
    template_name = 'app/index.html'

    def get_context_data(self, **kwargs):
        context = super(ArticleView, self).get_context_data(**kwargs)
        context['buses_table'] = ArticleListDatatable(self.request, ordering_column_index=0, **kwargs)
        return context
