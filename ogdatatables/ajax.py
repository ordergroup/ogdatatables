from __future__ import absolute_import, division, print_function

from dajaxice.decorators import dajaxice_register

from . import Datatable


@dajaxice_register
def reload_datatable(request, json,
                     page=None, on_page=None, query=None, ordering=None, ordering_ascending=True,
                     dropdown_filters=None, sortorder=None, sortremove=None, sorttoggle=None, sorting=None):
    # import time
    # time.sleep(5)
    try:
        cls, kwargs = Datatable.from_json(json)
        if not issubclass(cls, Datatable):
            return ''
        t = cls(page=page, on_page=on_page, query=query, ordering_column_index=ordering,
                ordering_ascending=ordering_ascending, dropdown_filters_values=dropdown_filters, sortorder=sortorder,
                sortremove=sortremove, sorttoggle=sorttoggle, sorting=sorting, request=request, **kwargs)
        t._include_container = False
        return t.render()
    except Exception as e:
        import traceback
        traceback.print_exc()
        raise e
